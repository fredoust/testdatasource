// Default empty project template
#ifndef TestDatasource_HPP_
#define TestDatasource_HPP_

#include <QObject>

namespace bb { namespace cascades { class Application; }}

/*!
 * @brief Application pane object
 *
 *Use this object to create and init app UI, to create context objects, to register the new meta types etc.
 */
class TestDatasource : public QObject
{
    Q_OBJECT
public:
    TestDatasource(bb::cascades::Application *app);
    virtual ~TestDatasource() {}
};


#endif /* TestDatasource_HPP_ */
