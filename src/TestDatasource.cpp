// Default empty project template
#include "TestDatasource.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/data/DataSource>

using namespace bb::cascades;
using namespace bb::data;

TestDatasource::TestDatasource(bb::cascades::Application *app)
: QObject(app)
{


	bb::data::DataSource::registerQmlTypes();
    // create scene document from main.qml asset
    // set parent to created document to ensure it exists for the whole application lifetime
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

    // create root object for the UI
    AbstractPane *root = qml->createRootObject<AbstractPane>();
    // set created root object as a scene
    app->setScene(root);
}

