// Default empty project template
import bb.cascades 1.0
import bb.data 1.0
// creates one page with a label
Page {
    Container {
        onCreationCompleted: dataSource.load();
        layout: DockLayout {}
            attachedObjects: [              
                ArrayDataModel {
                    id: dataModel
                   
                },
                DataSource {
                    id: dataSource
                           /*
                                        [
                                        {firstname:'Loren',lastname:'James'},
                                        {firstname:'Noel',lastname:'da Costa'},
                                        {firstname:'Genevieve',lastname:'Donson'},
                                        {firstname:'Mark',lastname:'Di Capri'},
                                        {firstname:'Sharl',lastname:'Temba'},
                                        ]       
                                        */
                    source: "" //your url here for exemple sending the json above
                    type: DataSourceType.Json
                    onError:{
                        
                        console.log(errorMessage)
                         
                    
                    }
                    onDataLoaded: {
                        console.log("test")
                        dataModel.clear();
                        var items=data
                        for(var i=items.length-1;i>-1;i--){
                            console.log(items[i].firstname)
                             //here your item, you can if you want fill a dataModel binded to a ListView
                            dataModel.append(items[i])
                        }
                    }
                }
            ]
    }
}

